import sys
sys.path.append('../')
import time
import os
import RPi.GPIO as GPIO
import time

sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from DFRobot_ADS1115 import ADS1115
ADS1115_REG_CONFIG_PGA_6_144V        = 0x00 # 6.144V range = Gain 2/3
ADS1115_REG_CONFIG_PGA_4_096V        = 0x02 # 4.096V range = Gain 1
ADS1115_REG_CONFIG_PGA_2_048V        = 0x04 # 2.048V range = Gain 2 (default)
ADS1115_REG_CONFIG_PGA_1_024V        = 0x06 # 1.024V range = Gain 4
ADS1115_REG_CONFIG_PGA_0_512V        = 0x08 # 0.512V range = Gain 8
ADS1115_REG_CONFIG_PGA_0_256V        = 0x0A # 0.256V range = Gain 16
ads1115 = ADS1115()

LED_value = [17]    #led gpio pin numbers

# Setup Pins
def setup():
    # Setup board mode
    GPIO.setmode(GPIO.BCM)

    # Setup regular GPIO
    for i in LED_value:
        GPIO.setup(i, GPIO.OUT)

if __name__ == "__main__":
    try:
        # Call setup function
        setup()

        while True :
            #Set the IIC address
            ads1115.set_addr_ADS1115(0x48)
            #Sets the gain and input voltage range.
            ads1115.set_gain(ADS1115_REG_CONFIG_PGA_6_144V)
            #Get the Digital Value of Analog of selected channel
            adc0 = ads1115.read_voltage(0)
            time.sleep(0.2)
            adc1 = ads1115.read_voltage(1)
            time.sleep(0.2)
            adc2 = ads1115.read_voltage(2)
            time.sleep(0.2)
            adc3 = ads1115.read_voltage(3)
            print("A0:%dmV A1:%dmV A2:%dmV A3:%dmV"%(adc0['r'],adc1['r'],adc2['r'],adc3['r']))
            if adc1['r'] >= 200:
                GPIO.output(LED_value[0], GPIO.HIGH)
                time.sleep(1)
            elif adc1['r'] <=50:
                GPIO.output(LED_value[0], GPIO.LOW)
                time.sleep(1)
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()
